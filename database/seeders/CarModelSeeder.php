<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = [
            ['A4', 'A6', 'A8', 'Q7'],
            ['X-1', 'X-3', 'X-5', 'X-6'],
            ['Camry', 'Land Cruiser', 'Corolla', 'RAV4'],
            ['Megane', 'Clio', 'Duster', 'Master'],
            ['Focus', 'Fusion', 'Transit', 'Kugo'],
            ['Astra', 'Vectra', 'Movano', 'Vivaro'],
            ['Civic', 'Accord', 'CR-V', 'NSX'],
        ];

        for ($j=0; $j<7; $j++) {
            for ($i = 0; $i < 4; $i++) {
                DB::table('car_models')->insert([
                    'title' => $models[$j][$i],
                    'brand_id' => $j+1
                ]);
            }
        }
    }
}

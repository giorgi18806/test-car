<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'До 1990 года выпуска',
            'От 1990 до 2000 года выпуска',
            'От 2000 до 2010 года выпуска',
            'После 2010 года выпуска',
        ];
        for ($i=0; $i<count($categories); $i++) {
            DB::table('car_categories')->insert([
                'years' => $categories[$i],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}

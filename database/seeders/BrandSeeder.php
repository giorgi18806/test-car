<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $brands = [
            'Audi',
            'BMW',
            'Toyota',
            'Renault',
            'Ford',
            'Opel',
            'Honda',
        ];
        for ($i=0; $i<count($brands); $i++) {
            DB::table('brands')->insert([
                'title' => $brands[$i],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}

<?php

use App\Http\Controllers\CarController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CarController::class, 'index'])->name('car.index');
//Route::get('/create', [CarController::class, 'create'])->name('car.create');
Route::get('findCarModelTitle', [CarController::class, 'findCarModelTitle']);
Route::resource('/cars', CarController::class);



<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCarRequest;
use App\Models\Brand;
use App\Models\Car;
use App\Models\CarModel;
use App\Services\CarService;
use Illuminate\Http\Request;

class CarController extends Controller
{
    private $carService;

    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }

    public function index()
    {
        $cars = Car::all();
        $brands = Brand::all();
        return view('car.index', compact('cars', 'brands'));
    }


    public function store(CreateCarRequest $request)
    {
        $car = $this
            ->carService
            ->create($request->validated());
        if ($car) {
            return redirect(route('car.index') )
                ->with('success', 'Автомобиль добавлен успешно.');
        } else {
            return back()->withErrors(['msd' => 'Ошибка сохранения.'])
                ->withInput();
        }

    }

    public function findCarModelTitle(Request $request)
    {
        $data = CarModel::where('brand_id', '=', $request->id)->get();
        return response()->json($data);
    }
}

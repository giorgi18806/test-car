<?php


namespace App\Services;


use App\Models\Car;
use App\Services\Base\MySqlService;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Database\Eloquent\Model;

class CarService extends MySqlService
{
    /**
     * @var int
     */

    public function create(array $data) {
        DB::beginTransaction();
        try {
            $data['car_category_id'] = $this ->category($data);

            $car = Car::create($data);
            DB::commit();

            return $car;
        } catch (Exception $exception) {
            $this->handleException($exception);
        }
    }

    public function category(array $data) {
        if ($data['year']<=1990) {
            return $category_id = 1;
        } elseif ($data['year']<=2000) {
            return $category_id = 2;
        } elseif ($data['year']<=2010) {
            return $category_id = 3;
        } else {
            return $category_id = 4;
        }
    }

}

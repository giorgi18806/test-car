@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card bg-light-grey">
            <div class="card-header">
                <div class="row justify-content-between">
                    <h3 class="float-left">Список Автомобилей</h3>
                        <button type="button" class="btn btn-outline-success right col-2" data-toggle="modal" data-target="#exampleModal">
                            Добавить Автомобиль
                        </button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form id="ajaxForm">
                                        @csrf
                                        <div class="card-body my-3 bg-light">
                                            <div class="form-group">
                                                <label for="brand_id">Марка Автомобиля</label>
                                                <select class="form-control" name="brand_id" id="brand_id">
                                                    <option selected disabled>Выберите марку автомобиля</option>
                                                    @foreach($brands as $brand)
                                                        <option {{ old('brand_id') == $brand ? "selected" : "" }} value="{{ $brand->id }}">{{ $brand->title }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger error" id="brandError"><small></small></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="car_model_id">Модель Автомобиля</label>
                                                <select class="form-control" name="car_model_id" id="car_model_id">
                                                    <option selected disabled>Выберите модель автомобиля</option>
                                                </select>
                                                <span class="text-danger error" id="modelError"><small></small></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="year">Год Выпуска</label>
                                                <input class="form-control" name="year" id="year" placeholder="введите год выпуска" value="{{ old('year') }}">
                                                <span class="text-danger error" id="yearError"><small></small></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="price">Цена</label>
                                                <input class="form-control" name="price" id="price" placeholder="укажите цену" value="{{ old('price') }}">
                                                <span class="text-danger error" id="priceError"><small></small></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="owner">Имя владельца</label>
                                                <input class="form-control" name="owner" id="owner" placeholder="введите имя" value="{{ old('owner') }}">
                                                <span class="text-danger error" id="ownerError"><small></small></span>
                                            </div>
                                            <div class="form-group my-2">
                                                <button type="button" class="btn btn-success save-data" id="submitForm" >Save</button>
{{--                                                <button type="submit" class="btn btn-primary" id="submitBttn" >Submit</button>--}}
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body" id="cardBody">
                <table id="myTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>##</th>
                        <th>Brand</th>
                        <th>Model</th>
                        <th>Year</th>
                        <th>Price</th>
                        <th>Owner</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $cars as $car)
                    <tr>
                        <td>{{ $car->id }}</td>
                        <td>{{ $car->brand->title }}</td>
                        <td>{{ $car->carModel->title }}</td>
                        <td>{{ $car->year }}</td>
                        <td>{{ $car->price }}</td>
                        <td>{{ $car->owner }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>##</th>
                        <th>Brand</th>
                        <th>Model</th>
                        <th>Year</th>
                        <th>Price</th>
                        <th>Owner</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
            $(document).on('change', '#brand_id', function (){
                let brand_id = $(this).val();
               let div = $(this).parent().parent();
               let op = "";

                $.ajax({
                    type: 'get',
                    url: '{!! URL::to('findCarModelTitle') !!}',
                    data:{'id':brand_id},
                    success:function (data) {
                        op+='<option value="0" selected disabled>Выберите модель автомобиля</option>';
                        for(let i=0; i<data.length; i++){
                            op+='<option value="'+data[i].id+'">'+data[i].title+'</option>';
                        }
                        div.find('#car_model_id').html(" ");
                        div.find('#car_model_id').append(op);
                    },
                    error:function(){

                    }
                })
            });

            $(document).on('click', '#submitForm', function(){
                $.ajax({
                    url: "cars",
                    type:"POST",
                    data:{
                        'brand_id': $('#brand_id').val(),
                        'car_model_id': $('#car_model_id').val(),
                        'year': $('#year').val(),
                        'price': $('#price').val(),
                        'owner': $('#owner').val(),
                        '_token': $('input[name=_token]').val(),
                    },
                    success:function(response) {
                        $('#cardBody').load(location.href + ' #cardBody')
                        $('#exampleModal').modal('hide');

                    },
                    error:function (response) {
                        $('#brandError').text(response.responseJSON.errors.brand_id);
                        $('#modelError').text(response.responseJSON.errors.car_model_id);
                        $('#yearError').text(response.responseJSON.errors.year);
                        $('#priceError').text(response.responseJSON.errors.price);
                        $('#ownerError').text(response.responseJSON.errors.owner);
                    }
                });

            });
        });
    </script>
@endsection
